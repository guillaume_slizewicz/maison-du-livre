Repository for the exhibition at Maison du Livre by An Mertens, Gijs de Heij and Guillaume Slizewicz

Three installation:
- Greffage d'arbres
- La voix au chapitre
- L'algolitterateur: barbarisme et glossolalie

Pour la voix au chapitre, on peut trouver dans ce dossier un notebook jupyter ainsi qu'un lien vers [un script qui est hébergé sur l'interface Colab](https://colab.research.google.com/drive/1gZd8asSPXkBple_f2T8LXm8K7LjE5RcZ?usp=sharing).



Les videos et sous titres du conseil communal sont issus du site internet de la commune.
