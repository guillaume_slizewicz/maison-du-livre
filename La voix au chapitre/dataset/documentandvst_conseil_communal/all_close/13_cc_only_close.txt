0:16:58.800 --> 0:17:01.080
On a fait, sur l'affaire
Conseil de police je crois,

0:17:01.080 --> 0:17:05.700
une plantation de 470 caméras qui
sont gérées par la zone de police.

0:17:05.800 --> 0:17:09.540
Mais en effet, il faut mettre un cadastre et
dire aux gens où il y a des caméras d'abord.

0:17:09.560 --> 0:17:11.700
Je crois que les caméras doivent être
signalées, que les gens puissent savoir

0:17:11.800 --> 0:17:14.060
que dans ces endroits-là, il y
a des caméras de surveillance

0:17:14.060 --> 0:17:17.200
même si celles-ci sont des caméras
qui sont tournées sur la voirie.

0:17:17.300 --> 0:17:21.860
Principalement, on voit les portes d'entrée.
Mais Madame la Présidente, moi pas de problème

0:17:22.000 --> 0:17:25.760
pour qu'on l'ait en face dans une section réunie,
peut-être avec la police qui explique ce qu'elle fait.

0:17:26.500 --> 0:17:29.340
Etablir le cadastre des caméras
pour que les gens puissent savoir

0:17:29.340 --> 0:17:31.560
où il y a des caméras dans la ville,
je trouve cela assez cohérent.

0:43:19.540 --> 0:43:23.400
Merci Madame la Présidente.

0:43:25.400 --> 0:43:29.880
Je comprends votre frustration

0:43:29.880 --> 0:43:33.700
et je ne vais pas pouvoir
beaucoup vous éclairer sur le futur.

0:43:33.700 --> 0:43:35.400
Je vais juste vous expliquer la situation.

0:43:36.040 --> 0:43:38.480
C’est vrai que depuis
un peu plus de deux ans,

0:43:39.160 --> 0:43:43.440
nous avons loué ce bâtiment à Citydev.

0:43:43.820 --> 0:43:46.200
Nous savions que c'était à terme un bâtiment,

0:43:46.200 --> 0:43:49.500
c'est normal, je trouve que l’administrateur
délégué de Citydev a raison

0:43:49.500 --> 0:43:51.260
lorsqu’il dit que
si l’on a des occupations précaires,

0:43:51.260 --> 0:43:54.840
elles disent bien ce qu’elles veulent dire,
il faut aussi montrer que l’on peut bouger.

0:43:55.420 --> 0:43:59.860
Je suis le premier à regretter ce à quoi
sont confrontés les riverains du parc,

0:43:59.940 --> 0:44:03.800
au-delà des personnes dans ce parc,
les riverains souffrent.

0:44:04.300 --> 0:44:07.380
Ma collègue Zoubida Jellab
s’y est encore rendue la semaine passée

0:44:07.380 --> 0:44:09.760
pour essayer d’entamer un dialogue
et ce n’est pas simple !

0:44:09.760 --> 0:44:12.320
Parce qu’ils ne croient plus
dans les institutions,

0:44:12.320 --> 0:44:14.740
ils ont un côté désœuvré
que l’on peut entendre.

0:44:14.740 --> 0:44:18.820
Je ne vais pas renoncer,
mais je comprends qu’ils se sentent abandonnés

0:44:19.080 --> 0:44:21.020
lorsque l’on voit ce qui se passe.

0:44:21.740 --> 0:44:23.420
Donc très clairement,

0:44:23.420 --> 0:44:27.160
vous rappelez un peu ce que nous avons fait
au-delà des réquisitions,

0:44:27.180 --> 0:44:29.120
dont nous avons aussi fait
pour d’autres associations

0:44:29.120 --> 0:44:33.460
Pour me rappeler
de ce que la ville a fait par le passé,

0:44:33.460 --> 0:44:35.240
puisque nous avons réquisitionné
des immeubles

0:44:35.240 --> 0:44:37.800
et non seulement
ceux des occupants du parc.

0:44:38.360 --> 0:44:41.300
Ici donc, c'est quand même
la location de ce bâtiment

0:44:41.480 --> 0:44:44.020
avec une série de frais qui ont été entamés

0:44:44.020 --> 0:44:46.120
pour plusieurs centaines
de milliers d’euros

0:44:46.120 --> 0:44:48.200
afin de remettre le bâtiment aux normes.

0:44:48.900 --> 0:44:52.440
C’est un service de gardiennage
puisque tous les soirs,

0:44:52.720 --> 0:44:56.560
des gardiens privés
viennent appuyer la plate-forme.

0:44:56.560 --> 0:44:58.600
Et je dirais même quasi tous les soirs,

0:44:58.940 --> 0:45:03.200
la police est là aussi
au cas où il y aurait du grabuge

0:45:03.200 --> 0:45:06.180
car ce n’est pas toujours
un public simple à gérer,

0:45:06.320 --> 0:45:09.300
notamment si c’est un public mixte.

0:45:09.300 --> 0:45:12.820
Tous les jours,
le CPAS et son service du linge

0:45:13.100 --> 0:45:15.480
emporte les linges pour les nettoyer

0:45:15.480 --> 0:45:18.060
ainsi que certains vêtements
des personnes logeant dans le bâtiment.

0:45:18.060 --> 0:45:20.780
Et ils redonnent du linge propre
tous les soirs,

0:45:21.100 --> 0:45:26.100
pour que le bâtiment
soit le plus accueillant possible

0:45:26.100 --> 0:45:29.260
parce qu’il est question de précarité,
je modère mes mots.

0:45:29.260 --> 0:45:34.120
Car, même s’il y a du travail extraordinaire
effectué par les associations,

0:45:34.120 --> 0:45:36.720
comme la ville de Bruxelles
qui fait ce qu'elle peut,

0:45:37.000 --> 0:45:38.680
cela reste quand même des accueils précaires

0:45:38.680 --> 0:45:40.680
et en effet, on préfèrerait
une solution structurelle.

0:45:40.840 --> 0:45:42.850
Nous avons aussi trouvé
des locaux pour les institutions

0:45:42.850 --> 0:45:45.570
...wachten zonder grenzen.

0:45:45.570 --> 0:45:48.260
pour qu’ils puissent remplir au mieux
leur mission d’information.

0:45:48.850 --> 0:45:52.180
Nous avons aussi trouvé un bâtiment
supplémentaire au mois de juillet

0:45:52.480 --> 0:45:55.220
avec ma collègue Karine Lalieux
pour le Samu social,

0:45:55.560 --> 0:45:59.550
mais dans l’objectif d’accueillir
les personnes supplémentaires du parc.

0:45:59.870 --> 0:46:01.460
C'est le bâtiment rue Royale.

0:46:01.460 --> 0:46:02.330
[Non destiné à l’audience]

0:46:02.620 --> 0:46:04.680
Rue Royale
avec nonante lits supplémentaires.

0:46:05.500 --> 0:46:11.080
Du côté de  la ville objectivement,
je veux  bien tout ce que l'on veut

0:46:11.080 --> 0:46:12.450
mais je ne sais pas aller
beaucoup plus loin.

0:46:12.450 --> 0:46:14.460
J’ai entendu Bruno de Lille le dire,

0:46:14.990 --> 0:46:17.300
j'ai écrit il y a 15 jours
à tous mes collègues

0:46:17.760 --> 0:46:23.470
pour dire à tous les Bourgmestres que
si chacun se mobilise un peu

0:46:23.880 --> 0:46:27.400
et prend une petite partie,
je ne crois pas du tout à l’appel d'air

0:46:27.400 --> 0:46:31.160
mais je crois que l'on pourra
quelque part répartir

0:46:31.160 --> 0:46:33.810
et soulager un peu
ce qui pourrait être fait,

0:46:33.810 --> 0:46:35.300
en tout cas pour l'accueil de nuit.

0:46:35.700 --> 0:46:36.510
Pour l’accueil de jour,

0:46:36.510 --> 0:46:38.130
il ne faut quand même pas
tourner autour du pot,

0:46:38.130 --> 0:46:41.300
la plupart des gens venus dans le parc
se trouvaient avant dans la gare,

0:46:41.300 --> 0:46:42.770
donc ce sont les mêmes personnes.

0:46:43.260 --> 0:46:46.000
A un moment donné,
nous avons décidé d’évacuer la gare,

0:46:47.170 --> 0:46:48.780
leur place n’est pas
spécialement dans une gare,

0:46:48.780 --> 0:46:50.490
je ne veux pas dire cela
mais il ne faut pas se tromper,

0:46:50.490 --> 0:46:52.620
ces gens ne disparaissent pas,
comme je le dis souvent.

0:46:52.990 --> 0:46:55.620
Vous m'avez interpellé sur le mot fait par Bravo,

0:46:55.620 --> 0:46:56.680
mais attention !

0:46:57.080 --> 0:46:59.290
Cela s’est fait un peu
en concertation avec les associations,

0:46:59.290 --> 0:47:01.460
parce qu’on avait un problème
de nettoyage du parc.

0:47:02.090 --> 0:47:04.070
Donc, nous avons demandé aux associations

0:47:04.070 --> 0:47:08.460
s’ils peuvent effectuer la distribution
de nourriture entre 5 h et 19h

0:47:08.930 --> 0:47:10.680
avec c'est vrai du matériel, bravo.
Pourquoi ?

0:47:10.680 --> 0:47:12.230
Car c'est *l’iceberg* de prévention

0:47:12.230 --> 0:47:14.760
et c'est elle qui a l'habitude
d'être en contact avec les associations.

0:47:14.940 --> 0:47:17.490
Aujourd'hui ce n'est pas la ville,
tout le monde sait que Bravo c'est la ville.

0:47:17.620 --> 0:47:20.290
La directrice de Bravo
connaît bien ce domaine

0:47:20.290 --> 0:47:22.180
puisqu’elle a travaillé avant
à « Fedasil».

0:47:22.310 --> 0:47:23.310
Nous avons pris un contact

0:47:23.310 --> 0:47:26.090
et je remercie aussi mes collègues
qui m’ont aidé à en établir,

0:47:26.270 --> 0:47:29.260
pour que justement je n’aille pas prendre
un arrêté d’interdiction

0:47:29.260 --> 0:47:30.740
pour la nourriture donnée
à certaines heures.

0:47:30.890 --> 0:47:33.380
Nous l’avons fait
avec intelligence pour essayer,

0:47:33.930 --> 0:47:38.560
c'est aussi par rapport aux  riverains,
leur dire que si à certaines heures,

0:47:38.560 --> 0:47:40.800
nous pouvons être
les services de la propreté

0:47:40.930 --> 0:47:43.370
et des espaces verts,
venir nettoyer le parc.

0:47:43.370 --> 0:47:45.280
Si les associations nous aident
à le nettoyer

0:47:45.280 --> 0:47:46.900
après que la nourriture soit distribuée,

0:47:47.050 --> 0:47:48.760
c'est dans un cadre un peu meilleur

0:47:48.760 --> 0:47:51.120
parce que vous dites vous-même
à la donne que vous y passez.

0:47:51.390 --> 0:47:53.340
Parfois l’Etat est quand même déplorable !

0:47:53.340 --> 0:47:56.380
Alors, il faut que l'on puisse trouver
des solutions par rapport à cela.

0:47:56.380 --> 0:48:00.940
Aujourd’hui, c'est vrai que
ce sont des jambes en bois.

0:48:01.410 --> 0:48:03.290
Je pense qu'il n’y a pas
une commune en Belgique

0:48:03.610 --> 0:48:06.740
ou une ville qui s’y investisse
autant que la ville de Bruxelles.

0:48:06.950 --> 0:48:09.280
Je pense que des solutions
structurelles politiques seraient possibles

0:48:09.280 --> 0:48:10.390
si l’on se mettait autour d'une table,

0:48:10.390 --> 0:48:13.620
malheureusement ce n’est pas possible
vu la situation politique.

0:48:14.350 --> 0:48:17.640
Il n’y a rien à dire pour l’instant,
car personne ne répond,

0:48:17.980 --> 0:48:19.910
je parle au niveau fédéral évidemment,

0:48:21.960 --> 0:48:24.980
à ce qu’il y ait normalement son rôle,
parce que chacun se renvoie la balle.

0:48:24.980 --> 0:48:27.620
En attendant, celle qui garde la balle
c’est la ville de Bruxelles,

0:48:27.620 --> 0:48:30.400
on ne l’a renvoyée à personne,
on a pris nos responsabilités.

0:48:30.960 --> 0:48:32.830
La région avait pris ses responsabilités,

0:48:32.830 --> 0:48:35.600
et je pense que maintenant
dans leur accord de Gouverne-Majorité,

0:48:35.600 --> 0:48:38.000
il y a clairement un passage là-dessus.

0:48:38.260 --> 0:48:45.020
J’ai encore rencontré le ministre
responsable de l’accueil aux personnes

0:48:45.020 --> 0:48:47.980
pour lui dire qu’il est important
de réunir les différentes communes

0:48:47.980 --> 0:48:51.220
et les différents acteurs afin de trouver
une solution structurelle.

0:48:51.500 --> 0:48:52.900
Pour le moment, cela n’existe pas !

0:48:53.020 --> 0:48:56.070
On ne va pas se mentir,
cette prolongation est précaire.

0:48:58.480 --> 0:49:01.750
Tout en réfléchissant à d'autres options,
on sait qu’en même temps,

0:49:02.320 --> 0:49:04.750
même si l’on devait trouver un bâtiment
et le réquisitionner,

0:49:04.750 --> 0:49:09.600
ce que j’ai déjà fait plus de cinq fois,
je crois, si je me souviens bien.

0:49:10.270 --> 0:49:12.230
Il faut quand même subvenir aux besoins

0:49:12.230 --> 0:49:13.940
soit du Samu, soit de la plate-forme

0:49:13.940 --> 0:49:15.710
pour qu’une personne puisse
gérer ce bâtiment.

0:49:15.910 --> 0:49:18.380
Ce n’est pas la ville
et son CPAS qui peuvent gérer

0:49:18.580 --> 0:49:21.200
l’accueil des gens dans le bâtiment,
on en serait incapable !

0:49:21.200 --> 0:49:22.580
Il faudrait quand même
être très clair là-dessus.

0:49:22.580 --> 0:49:24.910
Il est aussi question de financement,

0:49:25.340 --> 0:49:29.840
C’est un débat extrêmement complexe,
mais malheureusement aujourd’hui,

0:49:30.010 --> 0:49:32.500
vous me demandez beaucoup de réponses
et je n'en ai pas !

0:49:32.900 --> 0:49:38.090
Mais nous continuons à chercher, à solliciter
et à espérer qu’en effet,

0:49:38.940 --> 0:49:42.080
les forces positive se mobiliseront
pour trouver une solution

0:49:42.430 --> 0:49:44.660
qui soit plus structurelle
que ce qu'elle n'est,

0:49:44.660 --> 0:49:46.990
même si nous avons tenu
deux ans et demi, je trouve,

0:49:47.490 --> 0:49:49.560
avec la bonne volonté
des uns et des autres,

0:49:49.560 --> 0:49:51.560
avec les moyens qui ont pu être
fournies par la ville

0:49:51.560 --> 0:49:54.070
et la Région bruxelloise sur ce dossier.

0:49:54.070 --> 0:49:55.490
Voilà, je vous remercie.


2:37:39.450 --> 2:37:42.640
Pour madame Loulaji aussi pour
qu'on me réponde un peu en français,

2:37:42.660 --> 2:37:47.400
ce que je reconnais c'est que je n'ai pas
reçu ceux qui sont déjà investis

2:37:47.400 --> 2:37:50.500
depuis de nombreuses années
dans ce Conseil consultatif.

2:37:50.500 --> 2:37:53.140
Donc, je vais résoudre ça
en fixant une date rapidement.

2:37:55.040 --> 2:37:58.010
L'application d'ordonnance est à
renouvellement, mais on va quand même

2:37:58.010 --> 2:38:03.260
s'inspirer d'ordonnance, elle vient d'être voter.
Il s'agit de rester cohérent par rapport à

2:38:03.260 --> 2:38:07.320
ce qui est voté dans l'Assemblée régionale.
Et pour le reste, je vais recevoir, en effet,

2:38:07.320 --> 2:38:11.280
les membres historiques je dirais,
du Conseil consultatif, pour voir avec eux

2:38:11.280 --> 2:38:15.300
comment ils voient les choses par rapport à
l'évolution de cette matière.

2:38:15.300 --> 2:38:20.150
Sachez bien que la matière des séniors
fait partie de mes procurations prioritaires.

2:38:20.150 --> 2:38:23.350
Je n'ai pas choisi cette compétence par hasard,
dans le giron des compétences,

2:38:23.350 --> 2:38:27.350
que j'avais à choisir comme Bourgmestre.
- Voilà Madame la Présidente.

2:41:30.040 --> 2:41:33.160
Je vais donc vite organiser madame Loulaji,
une réunion.

2:41:33.160 --> 2:41:38.440
Monsieur, je disais que Monsieur Vanden Borre avait raison,
c'est vrai avant d'appliquer il faut sans doute les voir, mon cabinet a vu,

2:41:38.440 --> 2:41:42.220
mais que je devrai aller voir en personne,
je reconnais ne pas avoir déjà organisé ça.

2:41:42.220 --> 2:41:45.520
C'est une erreur, donc je vais le faire.
- Il faut prévoir une date limite par rapport à

2:41:45.520 --> 2:41:48.300
votre rentrontre avec eux. Comme ça,
au moins, ils savent où ils mettent les pieds,

2:41:48.300 --> 2:41:50.000
parce qu'ils sont plus désordonnés
par rapport à cela.

2:41:50.000 --> 2:41:53.940
- On va essayer d'appliquer l'ordonnance
pour installer le Conseil pour la fin de l'année.

2:41:53.940 --> 2:41:56.460
- C'est juste pour qu'ils sachent un peu comment
ils vont s'organiser aussi.

2:41:56.460 --> 2:41:59.020
- C'est vrai qu'on a été pris par plein de choses
On a eu des différents états généraux.

2:41:59.020 --> 2:42:02.220
On a encore regardé, concernant la réunion
de ce matin sur la participation en général

2:42:02.220 --> 2:42:06.000
et les conseils consultatifs en général
dont il faut qu'on parle aussi.

2:42:06.000 --> 2:42:08.300
Parce qu'il y a des conseils consultatifs
qui se renouvellent un peu comme ça.

2:42:10.640 --> 2:42:12.800
Mais il faut vraiment rencontrer ces gens
qui se sont investis

2:42:12.960 --> 2:42:16.180
pendant de nombreuses années,
je suis d'accord.


3:08:35.900 --> 3:08:39.560
Voilà, parce que je me suis engagé
et qu'à chaque éléction, et j'en fais beaucoup.

3:08:39.560 --> 3:08:44.020
Je mets le point NEO en 1 sur tous mes flyers.
Comme ça, si les gens votent pour moi,

3:08:44.020 --> 3:08:46.720
ils savent que je vais défendre NEO.
C'est le premier engagement que j'ai pris.

3:08:46.720 --> 3:08:50.620
Je peux le reprendre devant vous et le réaffirmer,
que je défendrai le projet NEO.

3:08:50.620 --> 3:08:54.420
Comme ça, c'est bien clair.
- Donc, vous m'avez demandé la distribution

3:08:54.420 --> 3:08:57.000
à toutes boîtes.
L'organisation des réunions d'information

3:08:57.000 --> 3:09:01.620
sur les PAD, était réalisée par perspective,
qui se charge de tous les aspects

3:09:01.620 --> 3:09:06.840
liés à la communication et aux invitations.
Pour la réunion du PAD d'Ezel pour le 17 septembre,

3:09:06.840 --> 3:09:11.040
les invitations ont été communiquées
sur le site internet de Perspective.

3:09:11.040 --> 3:09:17.020
Perspective a procédé à la distribution
de 12 500 flyers dans le périmètre dessiné

3:09:17.020 --> 3:09:23.500
et a publié le 02 et 03 septembre,
les journaux suivants: Le News Blast,

3:09:23.500 --> 3:09:28.300
le Last News, le Soir et la Capitale,
des avis concernant la réunion.

3:09:28.300 --> 3:09:32.140
Les modes de communication mises en oeuvre
pour diffuser l'information relative

3:09:32.140 --> 3:09:36.640
au PAD, sont organisées par la réglementation
relative à cet PAD, en l'occurrence,

3:09:36.640 --> 3:09:38.640
c'est la région qui les prépare
et veille à cette communication.

3:09:38.640 --> 3:09:41.320
Ceci étant dit la ville réservera
une suite favorable aux demandes,

3:09:41.320 --> 3:09:43.900
qui lui sont adressées par la région
pour relier les communications.

3:09:43.900 --> 3:09:48.460
Conformément à l'article 3, paragraphe 4,
de l'arrêté du 03 mai 2018,

3:09:48.460 --> 3:09:51.000
relatif au processus d'information
et participation du public,

3:09:51.000 --> 3:09:54.400
préalable élaboration des projets du Plan
d'Aménagement Directeur, les compte-rendu

3:09:54.400 --> 3:09:56.900
des débats tout le long de la réunion,
seront publiés sur le site

3:09:56.900 --> 3:10:01.460
de Perspective Brussel environ les 15 jours
après la date de réunion concernée.

3:10:01.460 --> 3:10:05.460
Le PV des réunions est donc systématiquement
rédigé peu après la réunion et mis en ligne

3:10:05.460 --> 3:10:10.600
sur le site internet de Perspective ainsi que
tous les autres documents en lien avec le PAD.

3:10:10.600 --> 3:10:15.320
Une fois encore, le PAD est un outil régional
la procédure est régie par une ordonnance.

3:10:15.320 --> 3:10:17.600
Donc, une loi.

3:10:17.600 --> 3:10:21.600
L'avis de la ville sera demandée dans le cas
de ces consultations qui sont prévues par la procédure.

3:10:21.600 --> 3:10:25.040
Ce type d'avis est une compétence du Collège,
mais si on s'interrogeait sur l'avis

3:10:25.040 --> 3:10:30.000
qui sera donné, nous répondrons évidemment
aux questions posées, cela va de soi.

